package homework4;

import phonebook.Notation;
import phonebook.PhoneDirectory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import static homework4.TestList.*;

public class Main {

    public static void main(String[] args) {
        ArrayList<String> listFruit = new ArrayList<>();
        listFruit.add("Mango");
        listFruit.add("Apple");
        listFruit.add("Banana");
        listFruit.add("Grapes");
        listFruit.add("Apple");
        listFruit.add("Apple");
        listFruit.add("Banana");
        listFruit.add("Melon");
        listFruit.add("Pear");
        listFruit.add("Melon");
        listFruit.add("Banana");
        listFruit.add("Mango");

        for (String fruit : listFruit) {
            System.out.println(fruit);
        }
        ArrayList <Node> listFindOccurrance = new ArrayList<>();
        listFindOccurrance = findOccurance(listFruit);
        System.out.println(listFindOccurrance);

        calcOccurance(listFruit);

        int occurance = countOccurance(listFruit, "Apple");
        System.out.println(occurance + " occurances in the listFruit.");


        Integer[] array = {2, 4, 6, 8, 8, 10, 10, 10};
        ArrayList<Integer> listNumbers = toList(array);
        System.out.println("Array: " + Arrays.toString(array));
        System.out.println("List: " + listNumbers);


        List<Integer> withoutDupes = findUnique(listNumbers);
        System.out.println("List without dupes: " + withoutDupes);

//********************************************************************************
        PhoneDirectory phoneDirectory = new PhoneDirectory();
        Notation notation1 = new Notation("Oksana", "+(380)123456789");
        Notation notation2 = new Notation("Anna", "+(380)123452323");
        Notation notation3 = new Notation("Oksana", "+(380)123456788");
        Notation notation4 = new Notation("Bob", "+(380)123456700");

        phoneDirectory.add(notation1);
        phoneDirectory.add(notation2);
        phoneDirectory.add(notation3);
        phoneDirectory.add(notation4);

        System.out.println(phoneDirectory);

       Notation n1 = phoneDirectory.find("Bob");
       System.out.println("Method find: " + n1);


      PhoneDirectory phoneDirectorySameName = phoneDirectory.findAll("Oksana");
      System.out.println("Method findAll: " + phoneDirectorySameName);
    }
}

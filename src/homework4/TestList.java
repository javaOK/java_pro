package homework4;

import java.util.*;
import java.util.stream.Collectors;

public class TestList {
    public static int countOccurance(ArrayList<String> listStr,String string){
        int count = 0;
        if(string == null || listStr.isEmpty()){
            return count;
        }
        for (String str:listStr) {
            if(str.equals(string)){
                count++;
            }
        }
        return count;
    }

    public static void calcOccurance(ArrayList<String> listStr){

        int calcOccurance = 0;
        String str;
        ArrayList<String> copyList = new ArrayList<>();
        copyList.addAll(listStr); // copying listStr to copylist
        Collections.sort(copyList);
        str = copyList.get(0);
        for (String temp:copyList) {
            if(str.equals(temp)){
                calcOccurance++;
            }
            else{
                System.out.println(str + ": " + calcOccurance);
                str = temp;
                calcOccurance = 1;
            }
        }
    }

    public static ArrayList<Node> findOccurance(ArrayList<String> listStr){
        ArrayList<Node> listOccurrance =new ArrayList<>();

        int calcOccurance = 0;
        String str;
        ArrayList<String> copyList = new ArrayList<>();
        copyList.addAll(listStr); // copying listStr to copylist
        Collections.sort(copyList);
        str = copyList.get(0);
        for (String temp:copyList) {
            if(str.equals(temp)){
                calcOccurance++;
            }
            else{
                listOccurrance.add(new Node(str,calcOccurance));
                str = temp;
                calcOccurance = 1;
            }
        }
        return listOccurrance;
    }

    public static ArrayList toList(Integer [] array){

        ArrayList arrayList = new ArrayList<>(Arrays.asList(array));

        return arrayList;
    }

    public static List findUnique(List<Integer> numbers){

        List<Integer> withoutDupes = numbers.stream()
                .distinct().collect(Collectors.toList());
        return withoutDupes;
    }

}

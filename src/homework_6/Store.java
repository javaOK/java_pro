package homework_6;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class Store {

    private List<Product> products;

    public Store() {
        products = new ArrayList<>();
    }

    public void addProduct(Product newProduct) {
        this.products.add(newProduct);
    }

    public List<Product> getBooks() {
        BigDecimal priceBigger = new BigDecimal("250");
        return this.products
                .stream()
                .filter(a -> (a.getType().equals(NameProduct.BOOK))
                        && ((a.getPrice().compareTo(priceBigger)) > 0))
                .collect(Collectors.toList());
    }

    public List<Product> isDiscount() {
        BigDecimal discount = new BigDecimal("0.9");
        return this.products
                .stream()
                .filter(a -> a.getType().equals(NameProduct.BOOK) && a.isDiscount())
                .peek(a -> a.setPrice(a.getPrice().multiply(discount).setScale(2, RoundingMode.HALF_UP)))
                .collect(Collectors.toList());
    }

    public List<Product> threeRecentProducts() {

        return this.products
                .stream()
                .sorted(Comparator.comparing(Product::getDate).reversed())
                .limit(3)
                .collect(Collectors.toList());
    }

    public Product cheapestProduct(NameProduct type) {

        return this.products
                .stream()
                .filter(a -> a.getType().equals(type))
                .min(Comparator.comparing((Product::getPrice)))
                .orElseThrow(() -> new NullPointerException("No product of type " + type));
    }

    public BigDecimal sumPrice() {
        BigDecimal priceSmaller = new BigDecimal("75");
        return this.products.stream()
                .filter(a -> a.getType().equals(NameProduct.BOOK)
                        && a.getPrice().compareTo(priceSmaller) < 0
                        && a.getDate().getYear() == LocalDate.now().getYear())
                .map(Product::getPrice)
                .reduce(BigDecimal::add).orElseThrow(() -> new NullPointerException("" +
                        "No books found that were added in the current year, the price of which is less than " +
                        priceSmaller));
    }

    public Map<NameProduct, List<Product>> groupByType() {

        return this.products.stream()
                .collect(Collectors.groupingBy(Product::getType));
    }

    @Override
    public String toString() {
        return "Store{" +
                "products=" + products +
                '}';
    }
}

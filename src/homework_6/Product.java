package homework_6;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

public class Product {

    private NameProduct type;
    private BigDecimal price;
    private boolean discount;
    private long id;
    private LocalDate date;

    public Product(NameProduct type, BigDecimal price, boolean discount, LocalDate date, long id) {
        this.type = type;
        this.price = price;
        this.discount = discount;
        this.id = id;
        this.date = date;
    }

    public NameProduct getType() {
        return type;
    }

    public void setType(NameProduct type) {
        this.type = type;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public boolean isDiscount() {
        return discount;
    }

    public void setDiscount(boolean discount) {
        this.discount = discount;
    }

    public LocalDate getDate() {
        return date;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Product{" +
                "type=" + type +
                ", price=" + price +
                ", discount=" + discount +
                ", id=" + id +
                ", date=" + date +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return discount == product.discount && id == product.id
                && type == product.type && Objects.equals(price, product.price)
                && Objects.equals(date, product.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, price, discount, id, date);
    }
}

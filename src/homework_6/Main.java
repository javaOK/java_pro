package homework_6;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;


public class Main {

    public static void main(String[] args) {
        Product product1 = new Product(NameProduct.BOOK, new BigDecimal("270.0"), false, LocalDate.of(2022, 11, 22), 33);
        Product product2 = new Product(NameProduct.TOY, new BigDecimal("90.0"), false, LocalDate.of(2023, 1, 30), 88);
        Product product3 = new Product(NameProduct.NOTEBOOK, new BigDecimal("14.0"), true, LocalDate.of(2023, 2, 7), 55);
        Product product4 = new Product(NameProduct.BOOK, new BigDecimal("26.0"), true, LocalDate.of(2023, 1, 20), 99);
        Product product5 = new Product(NameProduct.BOOK, new BigDecimal("55.0"), true, LocalDate.of(2020, 6, 20), 11);

        Store store = new Store();

        store.addProduct(product1);
        store.addProduct(product2);
        store.addProduct(product3);
        store.addProduct(product4);
        store.addProduct(product5);

        List<Product> books = store.getBooks();
        System.out.println(books);

//       List<Product> booksIsDiscount = store.isDiscount();
//       System.out.println(booksIsDiscount);

        Product book = store.cheapestProduct(NameProduct.BOOK);
        System.out.println("cheapest book: " + book);

        BigDecimal calcSumPrice = store.sumPrice();
        System.out.println(calcSumPrice);

        System.out.println(store.groupByType());
        System.out.println(store.threeRecentProducts());

    }
}

package java_pro_2;
import java_pro_1.Employee;

public class SameName {

    private Employee [] arrayEmployeers;
    final int MAX = 10;
    private int count;

    public SameName(){
        arrayEmployeers = new Employee[MAX];
        count = 0;
    }

    public void addEmployee(Employee e){
        if(count == 10)
            return;
        if(count == 0){    //array is empty
            arrayEmployeers[0] = new Employee(e);
            count++;
        }
        else{
            arrayEmployeers[count] = new Employee(e);
            count++;
        }
    }

}

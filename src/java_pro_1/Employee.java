package java_pro_1;

public class Employee {
    private String Last_FirstName_Father;
    private String position;
    private String email;
    private String phoneNumber;
    private int age;

    public Employee(String last_FirstName_Father, String position, String email, String phoneNumber, int age) {
        Last_FirstName_Father = last_FirstName_Father;
        this.position = position;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.age = age;
    }

    public Employee(Employee e) {   //copy contructor
        Last_FirstName_Father = e.getLast_FirstName_Father();
        this.position = e.getPosition();
        this.email = e.getEmail();
        this.phoneNumber = e.getPhoneNumber();
        this.age = e.getAge();
    }

    public Employee() { // constructor default
        this.Last_FirstName_Father = "A.B.C";
        this.position = "manager";
        this.email = "abc@gmail.com";
        this.phoneNumber = "+000000000000";
        this.age = 40;
    }

    public String getLast_FirstName_Father() {
        return Last_FirstName_Father;
    }

    public String getPosition() {
        return position;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "Last_FirstName_Father='" + Last_FirstName_Father + '\'' +
                ", position='" + position + '\'' +
                ", email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", age=" + age +
                '}';
    }

}

package homework_5;

import java.util.*;

public class FileNavigator {

    private Map<String, Set<FileData>> fileDataMap;

    public FileNavigator() {
        this.fileDataMap = new HashMap<>();
    }

    public void add(FileData fileData,String path) {
        if (fileData == null || path == null) {
            return;
        }
        else if(!fileData.getPath().equals(path)){
            System.out.println("Error!The Key-Path and the Path-to-file aren't matched.");
            return;
        }
        else {
            if(this.fileDataMap.isEmpty()){
                Set<FileData> setList = new HashSet<>();
                setList.add(fileData);
                this.fileDataMap.put(path,setList);
                return;
            }
            for (Map.Entry<String,Set<FileData>> map : this.fileDataMap.entrySet()) {
                if (map.getKey().equals(path)) {
                    (map.getValue()).add(fileData);
                    return;
                }
            }
            Set<FileData> setList = new HashSet<>();
            setList.add(fileData);
            this.fileDataMap.put(path, setList);;
        }
    }

    public Set<FileData> find(String path){

        Set<FileData> mapSameDirectory = new HashSet<>();
        for (Map.Entry<String,Set<FileData>> map : this.fileDataMap.entrySet()) {
            if(map.getKey().equals(path)) {
                mapSameDirectory.addAll(map.getValue());
                return mapSameDirectory;
            }
       }
        return mapSameDirectory;
    }

    public void remove(String path){

        if(path == null){
            return;
        }
        Iterator<Map.Entry<String,Set<FileData>>> iterator = this.fileDataMap.entrySet().iterator();
        while (iterator.hasNext()) {
            if (iterator.next().getKey().equals(path))
                iterator.remove();
            return;
        }
    }

    public Set<FileData> sortBySize(){

        Set<FileData> filesData = new TreeSet<>();
        for (Map.Entry<String, Set<FileData>> map : this.fileDataMap.entrySet()) {
            for (FileData fileData:map.getValue()) {
                filesData.add(fileData);
            }
        }
        return filesData;
    }
    public Set<FileData> filterBySize(int bytes) {
        Set<FileData> setFilterBySize = new HashSet<>();
        for (Map.Entry<String,Set<FileData>> map : this.fileDataMap.entrySet()) {
            for (FileData fileData:map.getValue()) {
                if (fileData.getSizeOfFile() <= bytes) {
                    setFilterBySize.add(fileData);
                }
            }
        }
        return setFilterBySize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FileNavigator that = (FileNavigator) o;
        return Objects.equals(fileDataMap, that.fileDataMap);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fileDataMap);
    }

    @Override
    public String toString() {
        return "FileNavigator{" +
                "fileDataMap=" + fileDataMap +
                '}';
    }
}
package homework_5;

import java.util.Objects;

 class FileData implements Comparable<FileData>{

    private String name;
    private String path;
    private  int sizeOfFile;

    public FileData(String name, String path, int sizeOfFile) {
        this.name = name;
        this.path = path;
        this.sizeOfFile = sizeOfFile;
    }
    public FileData(FileData fileData) {
        this.name = fileData.name;
        this.path = fileData.path;
        this.sizeOfFile = fileData.sizeOfFile;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FileData fileData = (FileData) o;
        return sizeOfFile == fileData.sizeOfFile && Objects.equals(name, fileData.name) && Objects.equals(path, fileData.path);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, path, sizeOfFile);
    }

    public String getName() {
        return name;
    }

    public String getPath() {
        return path;
    }

    public int getSizeOfFile() {
        return sizeOfFile;
    }


    @Override
    public String toString() {
        return "FileData{" +
                "name='" + name + '\'' +
                ", path='" + path + '\'' +
                ", sizeOfFile=" + sizeOfFile +
                '}';
    }

    @Override
    public int compareTo(FileData other) {

        return (this.sizeOfFile - other.getSizeOfFile());
    }
}

package homework_5;

import java.util.Map;
import java.util.Set;

public class Main {

    public static void main(String [] args){

        FileData file1 = new FileData("file.txt","/path/to/file/",58);
        FileData file2 = new FileData("firstApp.java","/path/to/",1000);
        FileData file3 = new FileData("App.java","/path/to/file/",5000);
        FileData file4 = new FileData("Class.java","/path/to/",500);


        FileNavigator fileNavigator = new FileNavigator();

        fileNavigator.add(file1,new String("/path/to/file/"));
        fileNavigator.add(file2,new String("/path/to/"));
        fileNavigator.add(file3,new String("/path/to/file/"));
        fileNavigator.add(file4,new String("/path/to/"));
        System.out.println(fileNavigator);

        Set<FileData> dataSet = fileNavigator.sortBySize();
        System.out.println("Method sortBySize: " + dataSet);

        Set<FileData> set = fileNavigator.filterBySize(1000);
        System.out.println("Method filterBySize: " + set);

        Set<FileData> set1 = fileNavigator.find("/path/to/file/");
        System.out.println("Method find: " + set1);

        fileNavigator.remove("/path/to/file/");
        System.out.println("Method remove: " + fileNavigator);
    }
}

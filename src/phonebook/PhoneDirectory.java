package phonebook;

import java.util.*;

public class PhoneDirectory {

    private LinkedList<Notation>  phoneDirectory ;

    public PhoneDirectory() {
        this.phoneDirectory = new LinkedList<>();
    }

    public void add(Notation notation){
        int size = this.phoneDirectory.size();
        if(this.phoneDirectory.isEmpty()){
            this.phoneDirectory.add(notation);
            return;
        }
        int index = 0;
        for (Notation temp:this.phoneDirectory) {
            if(notation.getName().compareTo(temp.getName()) < 0 ){
                this.phoneDirectory.add(index,notation);
                return;
            }
            index++;
        }

        if(size == this.phoneDirectory.size()) {
            this.phoneDirectory.add(notation);
        }

    }

    public Notation find(String name){

        for (Notation notation :this.phoneDirectory) {
            if(notation.getName().equals(name)){
                return notation;
            }
        }
        return null;
    }

    public PhoneDirectory findAll(String name){
        PhoneDirectory listNotation = new PhoneDirectory();
        for (Notation notation :this.phoneDirectory) {
            if(notation.getName().equals(name)){
                listNotation.add(notation);
            }
        }
        return listNotation;
    }

    @Override
    public String toString() {
        return "PhoneDirectory{" +
                "phoneDirectory:" + phoneDirectory +
                '}';
    }
}
